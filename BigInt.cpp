//Antal Krisztian-Tamas
//akim1784 511
#include <iostream>
#include "BigInt.h"

int * BigInt::deZero(int * num, int & size) const{

	int i = 0;

	while (num[i] == 0)i++;
	if (i == 0)return num;

	if (i == size) {
		
		int *new_num = new int[1];
		new_num[0] = 0;

		size = 1;

		delete[] num;

		return new_num;
	}
	else {

		int *new_num = new int[size - i];
		size = size - i;

		for (int j = 0; j < size; j++) {
			new_num[j] = num[i];
			i++;
		}

		delete[] num;

		return new_num;
	}
}

bool BigInt::isEqual(const BigInt &y)const{
	
	if (this->n != y.n) return false;

	for (int i = 0; i < this->n; i++) {
		if (this->numbers[i] != y.numbers[i]) return false;
	}
	
	return true;
}

void BigInt::shl()
{
	int *tmp = new int[this->n];

	for (int i = 0; i < this->n; i++) {
		tmp[i] = numbers[i];
	}

	delete[] numbers;

	numbers = new int[this->n + 1];
	this->n++;

	for (int i = 0; i < this->n - 1; i++) {
		numbers[i] = tmp[i];
	}

	numbers[this->n - 1] = 0;

	delete[] tmp;
}

BigInt::BigInt() {

	numbers = new int[1];
	n = 1;
	sign = 1;
	numbers[0] = 0;

}

// 	sign	<- sign of number
//	n 		<- size of numbers array
//	numbers	<- digits of the number
BigInt::BigInt(int sign, int n,const int *numbers) : n(n), sign(sign) {

	this->numbers = new int[n];

	for (int i = 0; i < n; i++) {
		this->numbers[i] = numbers[i];
	}

	this->numbers = deZero(this->numbers, this->n);
	
}

BigInt::BigInt(const BigInt &x) {

	sign = x.sign;
	n = x.n;

	this->numbers = new int[n];

	for (int i = 0; i < n; i++) {
		this->numbers[i] = x.numbers[i];
	}

}

bool BigInt::operator==(const BigInt & x) const{

	if (this->sign == x.sign && isEqual(x)) return true;
	else return false;

}

bool BigInt::operator>(const BigInt & x) const{
	
	if (this->n > x.n)return true;
	if (this->n < x.n)return false;

	for (int i = 0; i < this->n; i++) {
		if (this->numbers[i] > x.numbers[i]) return true;
		else if (this->numbers[i] < x.numbers[i]) return false;

	}
	
	return false;
}

bool BigInt::operator>=(const BigInt & x) const{
	return (*this == x || *this > x);
}

bool BigInt::operator<(const BigInt & x) const
{
	if (this->n < x.n)return true;
	if (this->n > x.n)return false;

	for (int i = 0; i < this->n; i++) {
		if (this->numbers[i] < x.numbers[i]) return true;
		else if (this->numbers[i] > x.numbers[i]) return false;
	}

	return false;
}

bool BigInt::operator<=(const BigInt & x) const
{
	return (*this==x || *this < x);
}

BigInt& BigInt::operator=(const BigInt & x){
	
	this->n = x.n;
	this->sign = x.sign;

	delete[] this->numbers;

	this->numbers = new int[this->n];

	for (int i = 0; i < this->n; i++) {
		this->numbers[i] = x.numbers[i];
	}

	return *this;
}

BigInt BigInt::operator-(){
	
	this->sign = -this->sign;
	return *this;
}

BigInt BigInt::operator-(const BigInt & x) const
{
	return this->substract(x);
}

BigInt& BigInt::operator-=(const BigInt & x){
	*this = *this - x;

	return *this;
}

BigInt BigInt::operator+(const BigInt & x) const{
	return this->add(x);
}

BigInt& BigInt::operator+=(const BigInt & x)
{
	*this = *this + x;

	return *this;
}

BigInt& BigInt::operator++()
{
	int *tmp = new int[1];
	tmp[0] = 1;
	BigInt t(1, 1, tmp);

	*this += t;

	return *this;
}

BigInt BigInt::operator*(const BigInt & x) const
{
	return this->multiply(x);
}

BigInt & BigInt::operator*=(const BigInt & x)
{
	*this = *this * x;
	
	return *this;
}

BigInt BigInt::operator/(const BigInt & x) const
{
	return this->divide(x);
}

BigInt & BigInt::operator/=(const BigInt & x)
{
	*this = *this / x;

	return *this;
}

BigInt BigInt::add(const BigInt &x)const{

	if (isEqual(x) && this->sign == -x.sign) return BigInt();	//if the abs value of two numbers is equal, but the signs are different => 0
	if (this->sign != x.sign) {									//if the signs are different => substract
		BigInt z(x);
		z = -z;
		return BigInt::substract(z);
	}
	
	int carry = 0, i=this->n-1, j=x.n-1;
	int new_size = (this->n > x.n) ? this->n+1 : x.n+1;
	int new_sign = this->sign;

	int *new_num = new int[new_size];

	for (int z = 0; z < new_size; z++) {
		new_num[z] = 0;
	}

	int k = new_size-1;
	
	while (i >= 0 || j >= 0) {

		if (i >= 0) {
			new_num[k] += this->numbers[i];
			i--;
		}

		if (j >= 0) {
			new_num[k] += x.numbers[j];
			j--;
		}


		new_num[k] += carry;

		carry = new_num[k] / 10;
		new_num[k] = new_num[k] % 10;

		k--;

	}

	if (carry != 0)new_num[0] += carry;

	new_num = deZero(new_num, new_size);

	BigInt y(new_sign, new_size, new_num);

	delete[] new_num;

	return y;

}

BigInt BigInt::substract(const BigInt &x)const{
	
	if (isEqual(x) && this->sign == x.sign) return BigInt();
	if (this->sign != x.sign) {											//if the signs are different add(-x)
		BigInt z(x);
		z = -z;
		return BigInt::add(z);
	}

	int new_sign;

	if (this->sign != x.sign) {
		new_sign = (*this > x) ? this->sign : x.sign;
	}
	else {
		new_sign = (*this > x) ? this->sign : -x.sign;
	}

	int new_size = (new_sign == this->sign) ? this->n : x.n;
	int k = new_size-1, i = this->n - 1, j = x.n - 1, borrow=0;
	
	int *new_num = new int[new_size];

	for (int z = 0; z < new_size; z++) {
		new_num[z] = 0;
	}

	int sw = 1;							//Suppose, that *this >= x, if not, the we multiply by -1 in every operation

	if (*this < x) sw = -1;

	while (i >= 0 || j >= 0) {
		
		if (i >= 0) {
			new_num[k] += this->numbers[i] * sw;
			i--;
		}

		if (j >= 0) {
			new_num[k] -= x.numbers[j] * sw;
			j--;
		}

		new_num[k] += borrow;

		if (new_num[k] < 0) {
			new_num[k] += 10;
			borrow = -1;
		}
		else {
			borrow = 0;
		}

		k--;
	}
	
	new_num = deZero(new_num, new_size);

	BigInt y(new_sign, new_size, new_num);

	delete[] new_num;

	return y;
}

BigInt BigInt::multiply(const BigInt &x)const{

	int carry = 0, shift = 0, i = this->n - 1;

	int new_size = this->n + x.n;
	int new_sign = this->sign*x.sign;
	int *new_num = new int[new_size];
	int *temp = new int[new_size];

	for (int k = 0; k < new_size; k++) {
		new_num[k] = 0;
		temp[k] = 0;
	}

	
	for (int j = x.n - 1; j >= 0; j--) {
		
		for (int i = this->n - 1; i >= 0; i--) {

			int res = (this->numbers[i] * x.numbers[j]) + carry;

			temp[new_size - 1 - shift - (this->n - 1 - i)] = res%10;
			carry = res / 10;

		}

		temp[new_size - 1 - shift - this->n] = carry;

		shift++;
		carry = 0;

		for (int k = new_size-1; k >= 0; k--) {
			new_num[k] += temp[k]+carry;
			temp[k] = 0;
			carry = new_num[k] / 10;
			new_num[k] = new_num[k] % 10;
		}

	}

	new_num = deZero(new_num, new_size);
	BigInt y(new_sign, new_size, new_num);

	delete[] temp;
	delete[] new_num;

	return y;

}

BigInt BigInt::divide(const BigInt & x)const{

	if (x.numbers[0] == 0) throw DivisionByZero();
	if (x > *this) return BigInt();
	
	BigInt R;
	BigInt tmp_x(x);			//unsigned division, copy created that is certainly positive
	tmp_x.sign = 1;

	int new_size = this->n;
	int *sz = new int[new_size];

	for (int i = 0; i < new_size; i++) {
		sz[i] = 0;
	}

	//unsigned integer division algorithm:

	for (int i = 0; i < this->n; i++) {
		R.numbers[R.n-1] = this->numbers[i];
		if (R >= tmp_x) {
			int r = 0;
			while (R >= tmp_x) {
				R -= tmp_x;
				r++;
			}
			sz[i] = r;
		}
		R.shl();
	}

	sz = deZero(sz, new_size);

	BigInt Q(this->sign*x.sign, new_size, sz);

	delete[] sz;

	return Q;
}

void BigInt::print()const{

	if (sign == -1) {
		if (numbers[0] != 0)std::cout << '-';
	}

	for (int i = 0; i < n; i++) {
		std::cout << numbers[i];
	}
}

BigInt::~BigInt()
{
	delete[] numbers;
}

std::ostream & operator<<(std::ostream & lhs, const BigInt & rhs)
{
	if (rhs.sign == -1) {
		if (rhs.numbers[0] != 0) {
			lhs << '-';
		}
	}

	for (int i = 0; i < rhs.n; i++) {
		lhs << rhs.numbers[i];
	}

	return lhs;
}
