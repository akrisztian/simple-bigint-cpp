//Antal Krisztian-Tamas
//akim1784 511

class BigInt {

	int *numbers;
	int n;
	int sign;

	int *deZero(int *num, int &size)const;			//delete 0s from front of number
	bool isEqual(const BigInt &y)const;				//unsigned comparison
	void shl();										//shift left

public:

	class DivisionByZero{};							//exception class

	BigInt();
	BigInt(int sign, int n, const int* number);
	BigInt(const BigInt &x);

	friend std::ostream& operator <<(std::ostream& lhs, const BigInt& rhs);
	friend std::istream& operator >>(std::istream& lhs, const BigInt& rhs);

	bool operator ==(const BigInt &x)const;			//signed comparison
	bool operator >(const BigInt &x)const;	
	bool operator >=(const BigInt &x)const;
	bool operator <(const BigInt &x)const;
	bool operator <=(const BigInt &x)const;

	BigInt& operator =(const BigInt &x);			//operators
	BigInt operator -();
	BigInt operator -(const BigInt &x)const;
	BigInt& operator -=(const BigInt &x);
	BigInt operator +(const BigInt &x)const;
	BigInt& operator +=(const BigInt &x);
	BigInt& operator ++();
	BigInt operator *(const BigInt &x)const;
	BigInt& operator *=(const BigInt &x);
	BigInt operator /(const BigInt &x)const;
	BigInt& operator /=(const BigInt &x);

	BigInt add(const BigInt &x)const;
	BigInt substract(const BigInt &x)const;
	BigInt multiply(const BigInt &x)const;
	BigInt divide(const BigInt &x)const;

	void print()const;

	~BigInt();

};
