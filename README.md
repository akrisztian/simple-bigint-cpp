Basic BigInt implementation in C++. Supports the 4 basic operations(+,-,*,/)
and can be printed using either a print() method or an output stream.